import java.util.ArrayList;

public class recursivité {

    public static boolean rechercher(int x, int[]t, int i) {
        if (i == t.length - 1) {
            return false;
        } else if (t[i] == x) {
            return true;
        } else {
            return rechercher(x, t, i+1);
        }
    }

    // t.lenght longueur du tableau

    public static int nbOccAux(int x, int []t, int i) {
        if (t.length == 0) {
            return 0;
        }
        else if (i == t.length - 1) {
            if (t[i] == x) {
                return 1;
            }
            else {
                return 0;
            }
        }
        else if (t[i] == x){
            return 1 + nbOccAux(x, t, i + 1);
        }
        return nbOccAux(x, t, i + 1);
    }

    public int nbOcc(int x, int[]t) {
        if (t.length == 0) {
            return 0;
        }
        return nbOccAux(x, t, 0);
    }


    // char []tab = {'a','b','a','b'};
    public static boolean estPalindromeAux(char []t, int debut, int fin) {
        if (debut >= fin) {
            return true;
        }
        return t[debut] == t[fin] && estPalindromeAux(t, debut + 1, fin - 1);
    }

    public static boolean estPalindrome(char []t) {
        if (t.length == 0) {
            return true;
        }
        else if (t.length == 1) {
            return true;
        }
        return estPalindromeAux(t, 0, t.length - 1);
    }

    public static boolean estCroissantAux(int indice, int []t) {
        System.out.println(indice);
        if (indice == t.length) {
            return true;
        }
        return t[indice] >= t[indice - 1] && estCroissantAux(indice + 1, t);
    }
    public static boolean estCroissant(int []t) {
        if (t.length == 0) {
            return true;
        }
        else {
            return estCroissantAux(t[0], t);
        }
    }

    public static int inf(int n) {
        // on dois mettre en place in pavage de Dominos de 2x1
        // la hauteur est fixé a 2 et la larguer depend de n

        if (n == 0) {
            return 1;
        }
        else if (n == 1) {
            return 1;
        }
        else {
            return inf(n - 1) + inf(n - 2);
        }
    }



    public static void resoudreAux(int n, int depart, int millieu, int arrive) {
        if (n == 1) {
            System.out.println(depart + "->" + arrive);
        }
        else {
            resoudreAux(n - 1, depart, arrive, millieu);
            System.out.println(depart + "->" + arrive);
            resoudreAux(n - 1, millieu, depart, arrive);
        }
    }

    public static void resoudre(int n) {
       if (n == 0) {
           System.out.println("Pas de disque");
       }
       else if (n == 1) {
           System.out.println("1->3");
       }
       else {
           resoudreAux(n, 1, 2, 3);
       }
    }


    public static void main(String[] args) {

        /* Partie 1 */

        /*int []t = {};

        System.out.println(nbOccAux(5, t, 0));
        */

        /* Partie 2*/

        /*char []tab = {'a','b','c', 'b','c'};


        System.out.println(estPalindrome(tab));*/

        /* Partie 3 */
        /*int []t = {1, 2,3,4,5,6};

        System.out.println(estCroissant(t));*/

        /* Partie 4 */

        /*System.out.println(inf(2));*/

        /* Partie 5 */
        resoudreAux(2,2,3,1);
        //resoudre(20);
    }

}
